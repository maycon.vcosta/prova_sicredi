package page;

import com.codeborne.selenide.Selenide.*;
import com.codeborne.selenide.*;

import static com.codeborne.selenide.Selenide.$;

public class Page {

    public void alterarTema(){

        $(Selectors.byCssSelector("#switch-version-select > option:nth-child(2)")).shouldHave(Condition.enabled).click();

    }

    public void clicarBotaoAddCustumer(){

        $(Selectors.byText("Add Customer")).shouldHave(Condition.visible).click();
    }

    public void preencherFormulario(){

        $("#field-customerName").setValue("Teste Sicredi");
        $("#field-contactLastName").setValue("Teste");
        $("#field-contactFirstName").setValue("Maycon Vitor");
        $("#field-phone").setValue("51 9999-9999");
        $("#field-addressLine1").setValue("Av Assis Brasil,3970");
        $("#field-addressLine2").setValue("Torre D");
        $("#field-city").setValue("Porto Alegre");
        $("#field-state").setValue("RS");
        $("#field-postalCode").setValue("91000-000");
        $("#field-country").setValue("Brasil");
        $(Selectors.byCssSelector("#field_salesRepEmployeeNumber_chosen > a > span")).click();
        $("#field-creditLimit").setValue("200");
        $(Selectors.byCssSelector("#form-button-save")).shouldBe(Condition.visible).click();

    }

    public void confirmarCadastro(){

        $("#report-success>p").shouldHave(Condition.text("Your data has been successfully stored into the database."));

    }

    public void clicarBotaoGoBack(){

        $("#save-and-go-back-button").shouldHave(Condition.visible).click();
    }

    public void pesquisarRegistro(){

        $("#gcrud-search-form > div.scroll-if-required > table > thead > tr.filter-row.gc-search-row > td:nth-child(3) > input")
                .setValue("Teste Sicredi");

    }

    public void clicarBotaoMore(){

        $("#gcrud-search-form > div.scroll-if-required > table > tbody > tr:nth-child(1) > td:nth-child(2) > div.only-desktops > div > button")
                .should(Condition.visible).click();

    }

    public void deletarRegistro(){

        $("#gcrud-search-form > div.scroll-if-required > table > tbody > tr:nth-child(1) > td:nth-child(2) > div.only-desktops > div > div > a.delete-row.dropdown-item > i")
                .should(Condition.visible).click();
        $("body > div.container-fluid.gc-container > div.row > div.delete-confirmation.modal.fade.in.show > div > div > div.modal-body > p")
                .shouldHave(Condition.text("Are you sure that you want to delete this record?"));
        $("body > div.container-fluid.gc-container > div.row > div.delete-confirmation.modal.fade.in.show > div > div > div.modal-footer > button.btn.btn-danger.delete-confirmation-button")
                .shouldHave(Condition.visible).click();

        $("div > span:nth-child(4) > p")
                .shouldHave(Condition.visible)
                .should(Condition.text("Your data has been successfully deleted from the database"));


    }




















}
