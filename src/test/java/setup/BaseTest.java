package setup;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import org.junit.jupiter.api.BeforeAll;


import static com.codeborne.selenide.WebDriverRunner.isChrome;

public  class BaseTest {

    @BeforeAll
    public static void SetupDefinitons(){

        isChrome();
        Configuration.startMaximized = true;
        Selenide.open("https://www.grocerycrud.com/demo/bootstrap_theme");


    }
}
