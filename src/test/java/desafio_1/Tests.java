package desafio_1;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selectors;
import com.codeborne.selenide.Selenide;
import org.junit.jupiter.api.Test;
import page.Page;
import setup.BaseTest;

import static com.codeborne.selenide.Selenide.$;

public class Tests extends BaseTest {

Page task = new Page();

    @Test

    public void primeiroDesafio(){

    task.alterarTema();
    task.clicarBotaoAddCustumer();
    task.preencherFormulario();
    task.confirmarCadastro();

    }

    @Test
    public void segundoDesafio(){

        task.alterarTema();
        task.clicarBotaoAddCustumer();
        task.preencherFormulario();
        task.confirmarCadastro();
        task.clicarBotaoGoBack();
        task.pesquisarRegistro();
        task.clicarBotaoMore();
        task.deletarRegistro();


    }





}
